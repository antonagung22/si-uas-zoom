<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\zoomController;
use App\Http\Controllers\loginController;
use App\Http\Controllers\registerController;
use App\Http\Controllers\dashboardController;
use App\Http\Controllers\forgotpassController;
use App\Http\Controllers\peminjamanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [loginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/', [loginController::class, 'authenticate']);

Route::post('/logout', [loginController::class, 'logout']);

Route::get('/forgotpass', [forgotpassController::class, 'index'])->name('forgotpass');

Route::get('/register', [registerController::class, 'index'])->name('register')->middleware('guest');
Route::post('/register', [registerController::class, 'create']);

Route::get('/stafdashboard', [dashboardController::class, 'stafindex'])->name('stafdashboard')->middleware('role');
Route::get('/dashboard', [dashboardController::class, 'index'])->name('dashboard')->middleware('role');

Route::get('/tampilZoom', [zoomController::class, 'index'])->name('zoomList')->middleware('auth');

Route::get('/createZoom', [zoomController::class, 'create'])->name('zoomCreate')->middleware('auth');
Route::post('/insertZoom', [zoomController::class, 'insert'])->name('zoomInsert');

Route::get('/editZoom/{id}', [zoomController::class, 'edit'])->name('zoomEdit')->middleware('auth');
Route::post('/updateZoom/{id}', [zoomController::class, 'update'])->name('zoomUpdate');

Route::get('/deleteZoom/{id}', [zoomController::class, 'delete'])->name('zoomDelete');

Route::get('/tampilPeminjaman', [peminjamanController::class, 'index'])->name('peminjamanList')->middleware('auth');

Route::get('/createPeminjaman', [peminjamanController::class, 'create'])->name('peminjamanCreate')->middleware('auth');
Route::post('/insertPeminjaman', [peminjamanController::class, 'insert'])->name('peminjamanInsert');

Route::get('/editPeminjaman/{id}', [peminjamanController::class, 'edit'])->name('peminjamanEdit')->middleware('auth');
Route::post('/updatePeminjaman/{id}', [peminjamanController::class, 'update'])->name('peminjamanUpdate');

Route::get('/deletePeminjaman/{id}', [peminjamanController::class, 'delete'])->name('peminjamanDelete');

Route::get('/clear-cache', function (){
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cleared";
});