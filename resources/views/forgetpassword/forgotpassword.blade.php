
<!DOCTYPE html>
<html lang="en" >
	<head>
		<meta charset="utf-8" />
		<title>
			SI Manajemen Zoom
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />

	</head>
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-3" id="m_login" style="background-color: #1a0d2a;">
				<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
					<div class="m-login__container">
					
					<div class="card" style="border-radius: 1rem; background-color: rgba(255,255,255,0.7)">
		  			<div class="container py-5 h-500">	  
					<div class="row d-flex justify-content-center align-items-center h-100">
				    <div class="col col-xl-10">
        				  
							<div class="m-login__logo">
								<img src="../../../assets/app/media/img/logos/umbrella.png" height="70px" weight="130px">
							</div>
						<div class="m-login__forget-password">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Lupa Kata Sandi ?
								</h3>
								<div class="m-login__desc">
									Masukan Email Anda untuk Me-Reset Kata sandi:
								</div>
							</div>
							<form class="m-login__form m-form" action="{{ route('forgotpass') }}" method="GET">

								@csrf

								<div class="form-group m-form__group">
									<input class="@error('email') is-invalid @enderror form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off" value="{{ old('email') }}" style="background-color: white !important;">
									@error('email')
										<div class="form-control-feedback">
											<span style="color:red">
												{{ $message }}
											</span>
										</div>
									@enderror
								</div>
								<div class="m-login__form-action">
									<button type="submit" {{-- id="m_login_forget_password_submit" --}} class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
										Request
									</button>
									&nbsp;&nbsp;
									<button type="reset" {{-- id="m_login_forget_password_cancel"  --}}class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom  m-login__btn">
										Batal
									</button>
								</div>
							</form>
						</div> 
						<div class="m-login__account">
							<span class="m-login__account-msg">
								Belum mempunyai akun ?
							</span>
							&nbsp;&nbsp;
							<a href="{{ route('register') }}" {{-- id="m_login_signup" --}} class="m-link m-link--light m-login__account-link">
								Daftar
							</a>
						</div>
					</div>

					</div>
					</div>
					</div>
					</div>


				</div>
			</div>
		</div>
		<script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
	    <script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/snippets/pages/user/login.js') }}" type="text/javascript"></script>
	</body>
</html>
