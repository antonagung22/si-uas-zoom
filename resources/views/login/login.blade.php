
<!DOCTYPE html>
<html lang="en" >
	<head>
		<meta charset="utf-8" />
		<title>
			Sistem Informasi Zoom
		</title>
		<meta name="description" content="Sistem Peminjaman Zoom">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />

	</head>
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-3" id="m_login" style="background-color: #1a0d2a;">
				<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
					<div class="m-login__container">
						@if(session()->has('sukses'))
							<div class="alert alert-success alert-dismissible fade show" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
								{{ session('sukses') }}
							</div>
						@endif

						@if(session()->has('loginerror'))
							<div class="alert alert-danger alert-dismissible fade show" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
								{{ session('loginerror') }}
							</div>
						@endif

						<div class="card" style="border-radius: 1rem; background-color: rgba(255,255,255,0.7)">
		  				<div class="container py-5 h-500">
						  
						 <div class="row d-flex justify-content-center align-items-center h-100">
      					  <div class="col col-xl-10">
        				  
						
						<div class="m-login__signin">
							<div class="m-login__head">
							<div class="m-login__logo">
								<img src="../../../assets/app/media/img/logos/umbrella.png" height="70px" weight="130px">
							</div>
								<h1 class="m-login__title">
									<b>SI Manajemen Zoom</b>
								</h1>
								<br>
								<h3 class="m-login__title">
									Masuk
								</h3>
							</div>
							<form class="m-login__form m-form" action="/" method="POST">

								@csrf

								<div class="form-group m-form__group">
									<input class="@error('email') is-invalid @enderror form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off" autofocus required value="{{ old('email') }}" style="background-color: white !important;">
									@error('email')
										<div class="form-control-feedback">
											<span style="color:red">
												{{ $message }}
											</span>
										</div>
									@enderror
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" required style="background-color: white !important;">
								</div>
								<div class="row m-login__form-sub">
									{{-- <div class="col m--align-left m-login__form-left">
										<label class="m-checkbox  m-checkbox--light">
											<input type="checkbox" name="remember">
											Ingat saya
											<span></span>
										</label>
									</div> --}}
									<div class="col m--align-right m-login__form-right">
										<a href="{{ route('forgotpass') }}"  class="m-link">
											Lupa Password ?
										</a>
									</div>
								</div>
								<div class="m-login__form-action">
									<button type="submit" {{-- id="m_login_signin_submit" --}}class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
										Masuk
									</button>
								</div>
							</form>
						</div>
						
						{{-- <div class="m-login__signup">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Sign Up
								</h3>
								<div class="m-login__desc">
									Enter your details to create your account:
								</div>
							</div>
							<form class="m-login__form m-form" action="">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Fullname" name="fullname">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="password" placeholder="Password" name="password">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Confirm Password" name="rpassword">
								</div>
								<div class="row form-group m-form__group m-login__form-sub">
									<div class="col m--align-left">
										<label class="m-checkbox m-checkbox--light">
											<input type="checkbox" name="agree">
											I Agree the
											<a href="#" class="m-link m-link--focus">
												terms and conditions
											</a>
											.
											<span></span>
										</label>
										<span class="m-form__help"></span>
									</div>
								</div>
								<div class="m-login__form-action">
									<button id="m_login_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
										Sign Up
									</button>
									&nbsp;&nbsp;
									<button id="m_login_signup_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom m-login__btn">
										Cancel
									</button>
								</div>
							</form>
						</div>
						<div class="m-login__forget-password">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Forgotten Password ?
								</h3>
								<div class="m-login__desc">
									Enter your email to reset your password:
								</div>
							</div>
							<form class="m-login__form m-form" action="">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
								</div>
								<div class="m-login__form-action">
									<button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
										Request
									</button>
									&nbsp;&nbsp;
									<button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom  m-login__btn">
										Cancel
									</button>
								</div>
							</form>
						</div> --}}
						<div class="m-login__account">
							<span class="m-login__account-msg">
								Belum punya akun ?
							</span>
							&nbsp;&nbsp;
							<a href="{{ route('register') }}" {{-- id="m_login_signup" --}} class="m-link m-link--light m-login__account-link">
								Daftar
							</a>
						</div>

						</div>
						</div>
						</div>
					</section>
					</div>
				</div>
			</div>
		</div>
		<script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
	    <script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/snippets/pages/user/login.js') }}" type="text/javascript"></script>
	</body>
</html>
